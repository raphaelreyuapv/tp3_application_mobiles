package fr.uavignon.ceri.tp3.data.webservice;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface OWMInterface {
    @GET("data/2.5/weather")
    public Call<WeatherResponse> getWeather(@Query("q") String cityname,
                                            @Query("appid") String apikey);

}
