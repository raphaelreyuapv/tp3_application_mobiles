package fr.uavignon.ceri.tp3.data.webservice;

import java.util.List;

import fr.uavignon.ceri.tp3.data.City;

public class WeatherResult {
    public final boolean isLoading;
    public final Throwable error;

    public WeatherResult(boolean isLoading, Throwable error) {
        this.isLoading = isLoading;
        this.error = error;
    }

    public static void transferInfo(WeatherResponse weatherInfo, City cityInfo)
    {
        cityInfo.setTempKelvin(weatherInfo.main.temp);
        cityInfo.setHumidity(weatherInfo.main.humidity);
        cityInfo.setWindSpeedMPerS((Math.round(weatherInfo.wind.speed)));
        cityInfo.setIcon(weatherInfo.weather.get(0).icon);



    }
}
