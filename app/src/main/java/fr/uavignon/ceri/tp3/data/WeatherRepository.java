package fr.uavignon.ceri.tp3.data;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import fr.uavignon.ceri.tp3.data.database.CityDao;
import fr.uavignon.ceri.tp3.data.database.WeatherRoomDatabase;
import fr.uavignon.ceri.tp3.data.webservice.OWMInterface;
import fr.uavignon.ceri.tp3.data.webservice.WeatherResponse;
import fr.uavignon.ceri.tp3.data.webservice.WeatherResult;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

import static fr.uavignon.ceri.tp3.data.database.WeatherRoomDatabase.databaseWriteExecutor;

public class WeatherRepository {

    private static final String TAG = WeatherRepository.class.getSimpleName();

    private LiveData<List<City>> allCities;
    private MutableLiveData<City> selectedCity;

    private CityDao cityDao;
    private final OWMInterface api;

    private static volatile WeatherRepository INSTANCE;

    public synchronized static WeatherRepository get(Application application) {
        if (INSTANCE == null) {
            INSTANCE = new WeatherRepository(application);
        }

        return INSTANCE;
    }

    public WeatherRepository(Application application) {
        WeatherRoomDatabase db = WeatherRoomDatabase.getDatabase(application);
        cityDao = db.cityDao();
        allCities = cityDao.getAllCities();
        selectedCity = new MutableLiveData<>();
        isLoading = new MutableLiveData<>();
        Retrofit retrofit=
                new Retrofit.Builder()
                        .baseUrl("https://api.openweathermap.org")
                        .addConverterFactory(MoshiConverterFactory.create())
                        .build();

        api = retrofit.create(OWMInterface.class);
    }

    public LiveData<List<City>> getAllCities() {
        return allCities;
    }

    public MutableLiveData<City> getSelectedCity() {
        return selectedCity;
    }

    public MutableLiveData<Boolean> isLoading;

    public void loadWeatherCity(City city){
        isLoading.postValue(true);
        final MutableLiveData<WeatherResult> result = new MutableLiveData<WeatherResult>();
        result.setValue(new WeatherResult(true,null));
            api.getWeather(city.getName(),"d6b65ffc3d12dd81a3cde6714e9bf46e").enqueue(
                    new Callback<WeatherResponse>(){
                        @Override
                        public void onResponse(Call<WeatherResponse> call,
                                               Response<WeatherResponse> response){
                            Log.d("MWEIN","all good");
                            isLoading.postValue(false);
                            //result.postValue(new WeatherResult(false,null));
                            WeatherResult.transferInfo(response.body(),city);
                            updateCity(city);

                        }

                        @Override
                        public void onFailure(Call<WeatherResponse> call, Throwable t) {
                            Log.d("zzz",t.toString());
                            isLoading.postValue(false);
                            result.postValue(new WeatherResult(false, null));
                        }
                    });

    }
    public void loadWeatherAllCities(){
        List<City> cityList = cityDao.getSynchrAllCities();
        isLoading.postValue(true);
            for (City c : cityList) {
                final MutableLiveData<WeatherResult> result = new MutableLiveData<WeatherResult>();
                result.postValue(new WeatherResult(true, null));
                api.getWeather(c.getName(), "d6b65ffc3d12dd81a3cde6714e9bf46e").enqueue(
                        new Callback<WeatherResponse>() {
                            @Override
                            public void onResponse(Call<WeatherResponse> call,
                                                   Response<WeatherResponse> response) {
                                Log.d("MWEIN", "all good");
                                //result.postValue(new WeatherResult(false,null));
                                WeatherResult.transferInfo(response.body(), c);
                                updateCity(c);

                            }

                            @Override
                            public void onFailure(Call<WeatherResponse> call, Throwable t) {
                                Log.d("zzz", t.toString());

                                result.postValue(new WeatherResult(false, null));
                            }
                        });
            }
        isLoading.postValue(false);
    }
    public long insertCity(City newCity) {
        Future<Long> flong = databaseWriteExecutor.submit(() -> {
            return cityDao.insert(newCity);
        });
        long res = -1;
        try {
            res = flong.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (res != -1)
            selectedCity.setValue(newCity);
        return res;
    }

    public int updateCity(City city) {
        Future<Integer> fint = databaseWriteExecutor.submit(() -> {
            return cityDao.update(city);
        });
        int res = -1;
        try {
            res = fint.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (res != -1)
            selectedCity.setValue(city);
        return res;
    }

    public void deleteCity(long id) {
        databaseWriteExecutor.execute(() -> {
            cityDao.deleteCity(id);
        });
    }

    public void getCity(long id)  {
        Future<City> fcity = databaseWriteExecutor.submit(() -> {
            Log.d(TAG,"selected id="+id);
            return cityDao.getCityById(id);
        });
        try {
            selectedCity.setValue(fcity.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


}
