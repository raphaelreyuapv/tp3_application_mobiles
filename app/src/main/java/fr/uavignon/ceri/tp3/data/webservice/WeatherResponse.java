package fr.uavignon.ceri.tp3.data.webservice;

import java.util.List;

public class WeatherResponse{
    public final Main main=null;
    public final Wind wind=null;
    public final List<Detail> weather=null;

            public static class Main{
                    public final Float temp=null;
                    public final Integer humidity=null;

            }

            public static class Wind{
                    public final Float speed=null;
                    public final Integer deg=null;
            }
            public static class Detail{
                    public final String icon=null;
            }

}